# **ECCP 2021 Website Documentation**

## Homepage Header Carousel
- The code block below shows the structure of one slide.

```
<div class="item slide">
    <div class="container">
        <div class="slide-detail">
            <!-- caption placement on default (center) -->
            <div class="caption">
                <!-- YOUR TEXT HERE -->
            </div>
            <button class="cta button-secondary">
                Button Secondary
            </button>
        </div>
        <div class="media">
            <!-- YOUR MEDIA (VIDEO OR IMAGE HERE) -->
        </div>
    </div>
</div>
```

### Changing alignment of the captions
  - The default alignment is centered.
  - To change the alignment of the caption, just add `left` or `right` class to `<div class="slide-detail">`
### Adding Media
  - You can use Images or Videos as a background in your slide.
  - For images, simply add `<img src="yourimage.jpg">`
  - For videos, use the structure below

```
<video controls="false" autoplay="autoplay" loop="loop" muted="muted">
  <source src="yourvideo.mp4" type="video/mp4">
</video>
```

## Buttons
- There are 3 types of buttons, Primary, Secondary, and Accent. They are styled according to the primary, secondary, and accent colors set.
- use the classes `button-primary`, `button-secondary`, and `button-accent` accordingly.

## Colors
- When changing color schemes, simply change the values in the `_variables.scss` file.

## Fonts
- When changing fonts and typefaces, simply change the values in the `_fonts.scss` file.
- Just add the font file in the `assets/fonts` folder and add it to the `_fonts.scss` file using the following structure:

```
@font-face{
    font-family: 'FontName';
    src: url('../../fonts/yourfontfile.ttf');
}
```

- When importing fonts using a URL, simply add the line `@import url('https://yourimportedfonturl.com');` and you can directly use that font.
- Heading and body fonts are set in the `_fonts.scss` file. Just replace the values of `$type-heading` and `$type-body` accordingly.

## Widgets
- Edit widgets accordingly. The following table shows the widgets and their file names:

  | Widget | File name |
  |-|-|
  | Latest News | `_widget-news.njk` |
  | Events| `_widget-events.njk` |
  | Calendar| `_widget-calendar.njk` |

## 