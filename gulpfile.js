var gulp = require('gulp');
var nunjucksRender = require('gulp-nunjucks-render');
var watch = require('gulp-watch');
 
gulp.task('stream', function () {
    // Endless stream mode
    return watch('src/pages/*', { ignoreInitial: false })
    .pipe(
        nunjucksRender({
            path: [
                "src/templates",
                "src/partials",
                "src/pages"
            ],
            watch: true,
        })
    )
    .pipe(gulp.dest("dist"));
});


gulp.task('default', function () { 
    return gulp.src("src/pages/*")
    .pipe(
        nunjucksRender({
            path: [
                "src/templates",
                "src/partials",
                "src/pages"
            ],
            watch: true,
        })
    )
    .pipe(gulp.dest("dist"));
});