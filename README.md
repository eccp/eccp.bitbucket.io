### Install dependencies:
- Gulp: `npm install gulp`
- Gulp Nunjucks Renderer `npm install gulp-nunjucks-render --save-dev`

### Compile source codes
- Pages: run `gulp stream` to compile pages automatically
- Components: run `gulp` when you make changes to the components (sometimes `gulp stream` ignores the changes)
- Sass: run `sass --watch src:dist` inside _css_ folder

