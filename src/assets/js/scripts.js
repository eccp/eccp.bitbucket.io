$(document).ready(function(){
    $('#home-carousel').owlCarousel({
        loop:true,
        nav: true,
        dots: true,
        responsive:{
            0:{
                items:1
            }
        }
    })

    $('#testimonials-carousel').owlCarousel({
        loop:true,
        autoplay: true,
        nav: false,
        dots: true,
        responsive:{
            0:{
                items:1
            }
        }
    })

    $('#partners-carousel').owlCarousel({
        loop:true,
        autoplay: true,
        nav:true,
        dots: false,
        responsive:{
            0:{
                items:1
            },
            992:{
                items: 3
            },
            1200:{
                items: 6
            }
        }
    });

    $('#premium-carousel').owlCarousel({
        loop:true,
        autoplay: true,
        autoplayTimeout: 2000,
        nav:false,
        dots: false,
        responsive:{
            0:{
                items:1
            },
            768:{
                items: 2
            },
            992:{
                items: 4
            },
            1200:{
                items: 7
            }
        }
    });
    
  });